'use strict'

const { Server } = require('./models.js')
const db = require('../connectDB')
db.connect().then(async v => {
  const a = await Server.findOne({ id: '303152584846213120' })

  if (!a.roleAliases) {
    a.roleAliases = []
  }
  
  a.roleAliases.push({
    name: 'rgb',
    roles: []
  })

  a.markModified("roleAliases")
  a.save()
  .then(v => {
    console.log('saved')
    db.disconnect()
  })
  .catch(console.log)
})