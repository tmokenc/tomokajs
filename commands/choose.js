'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const errLog = debug('bot:err:command:choose')

function choose(message, { content }) {
  return new Promise(async (resolve, reject) => {
    if (!content) {
      return reject('Please enter something')
    }
    const a = content.split('|')
    const text = `I choose **${a[Math.floor(Math.random() * a.length)].trim()}**`

    resolve(text)
  })
}

const help = Help.init('choose')
  .setAliases(['random'])
  .setCountDown(2500)
  .stable()

exports.run = choose
exports.help = help


;
