'use strict'

const { Help } = require('../utils/help.js')

const jail = () => ({
  image: {
    url: "https://cdn.discordapp.com/attachments/512404979772948489/549829129906814976/RGB_in_jail.png"
  },
  color: 0xffffff * Math.random() << 0
})

const handler = () => Promise.resolve({ embed: jail() })

const help = Help.init('jail')
  .setDescription('The jail master')
  .setCountDown(1000)
  .stable()

exports.run = handler
exports.help = help
exports.main = jail

;
