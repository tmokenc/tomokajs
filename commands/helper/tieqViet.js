'use strict'

function tieqViet(tiengViet) {

  const key = {
    'c(?!(h|H))|q': 'k',     'C(?!(h|H))|Q': 'K',
    'c(h|H)|t(r|R)': 'c',    'C(h|H)|T(r|R)': 'C',
    'd|g(i|I)|r': 'z',       'D|G(i|I)|R': 'Z',
    'g(i|í|ì|ĩ|ỉ|ị)': 'z$1', 'G(i|í|ì|ĩ|ỉ|ị)': 'Z$1',
    'Đ': 'D',                'đ': 'd',
    'G(h|H)': 'G',           'g(h|H)': 'g',
    'p(h|H)': 'f',           'P(h|H)': 'F',
    'n((g|G)(h|H)?)': 'q',   'N((g|G)(h|H)?)': 'Q',
    'k(h|H)': 'x',           'K(h|H)': 'X',
    't(h|H)': 'w',           'T(h|H)': 'W',
    'n(h|H)': 'n\'',         'N(h|H)': 'N\''
  }

  for (const k in key) {
    tiengViet = tiengViet.replace(new RegExp(k, 'g'), key[k]);
  }

  return tiengViet
}

module.exports = tieqViet

//console.log(tieqViet('Cộng hòa xã hội chủ nghĩa Việt Nam'))
//console.log(tieqViet('Thống trị thế giới'))

;