'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')
const { matchEmoji } = require('../utils/regex.js')

const log = debug('bot:command:respect')

const respect = (msg, content) => {
  let sentence = '**' + msg.member.displayName + '** has paid their respects'
  
  content = content.replace(matchEmoji, '').trim()

  if (content) {
    sentence += ` for **${content}**`
  }

  log(sentence)

  return sentence
}

const handler = (message, { content }) => Promise.resolve(respect(message, content))



const help = Help.init('respect')
  .setAliases(['f', 'rip'])
  .setDescription('Press F to respect')
  .stable()

exports.run = handler
exports.help = help
exports.main = respect

;
