'use strict'

const debug = require('debug')
const { Help } = require('../utils/help.js')

const log = debug('bot:command:setPresence')

const validType = ["playing", "streaming", "listening", "watching"]
const validStatus = ['online', 'idle', 'invisible', 'dnd']

function parsePresence(content, presence = {}) {
  const [name, ...args] = content.split('--').map(v => v.trim())

  if (!presence.game) {
    presence.game = {}
  }

  if (name) {
    presence.game.name = name
  }

  for (const arg of args) {
    let [typ, ...value] = arg.split(' ')
    value = value.join(' ')

    switch (typ.toLowerCase()) {
      case 'name':
        presence.game.name = value
        break
      
      case 'url':
        presence.game.url = value
        break
      
      case 'type':
        if (!validType.includes(value.toLowerCase())) break
      
        presence.game.type = value.toUpperCase()
        break
      
      case 'status':
        value = value.toLowerCase()
        if (!validStatus.includes(value)) break

        presence.status = value
        break

      default:
        break
    }
  }

  return presence
}

const setPresence = (client, content) => {
  const last = client.user.presence
  const presence = parsePresence(content, last)

  log(presence)
  client.user.setPresence(presence)
}

const handler = async (message, { content }) => {
  setPresence(message.client, content)
  return
}



const help = Help.init('setPresence')
  .setDescription('set my presence')
  .setAliases(['setActivity'])
  .forAuthor()
  .canHandle()
  .stable()

exports.run = handler
exports.help = help
exports.main = setPresence

;
