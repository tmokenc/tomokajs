const matchEmoji   = /<:(\w+):(\d+)>/g
const matchChannel = /<#(\d+)>/g
const matchMember  = /<@(\d+)>/g
const matchRole    = /<@&(\d+)>/g

module.exports = {
    matchEmoji,
    matchChannel,
    matchMember,
    matchRole
}