'use strict'

const { Server } = require('../db/models.js')
const ORIGINAL = '418811018244784129'
const ADD = '303152584846213120'

async function add(client) {
  const server = await Server.findOne({ id: ORIGINAL })
  const targetServer = await Server.findOne({ id: ADD })

  
  const rgbIndex = server.roleAliases.findIndex(v => v.name === 'rgb')
  const rgbIndexTarget = targetServer.roleAliases.findIndex(v => v.name === 'rgb')
  
  const roles = server.roleAliases[rgbIndex].roles
    .reduce((v, x) => (
      v.find(e => e.name === x.name) ? v : v.concat([x])
    ), [])

  targetServer.roleAliases[rgbIndexTarget].roles = []
  
  const target = client.guilds.get(ADD)
  
  
  for (const [_, role] of target.roles) {
    if (roles.find(x => x.name === role.name)) {
      await role.delete()
      console.log(`deleted role ${role.name} with id ${role.id}`)
    }
  }
  
  
  for (const role of roles) {
    try {
      const { id, name, color } = await target.createRole({
        name: role.name,
        color: role.color,
        mentionable: true
      })

      console.log(`Created role ${name} with the id ${id}`)

      targetServer
        .roleAliases[rgbIndexTarget]
        .roles
        .push({ id, name, color })

    } catch (err) {
      console.log(err)
    }
  }

    targetServer.markModified("roleAliases")
    targetServer.save()
    .then(() => console.log('saved'))
    .catch(console.log())

  
}
module.exports = add