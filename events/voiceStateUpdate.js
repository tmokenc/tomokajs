module.exports = (oldMember, newMember) => {

    //Emitted whenever a user changes voice state - e.g. joins/leaves a channel, mutes/unmutes.

    //Params

    //The member before the voice state update 
    //The member after the voice state update 

}