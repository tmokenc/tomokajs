module.exports = (channel, time) => {

    //Emitted whenever the pins of a channel are updated. Due to the nature of the WebSocket event, not much information can be provided easily here - you need to manually check the pins yourself.

    //Params

    //The channel that the pins update occured in 
    //The time of the pins update 

}